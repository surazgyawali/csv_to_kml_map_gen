import simplekml

def generate_style_map(color_list,categories):
    style_dict = {}
    icon_href = 'http://www.gstatic.com/mapspro/images/stock/959-wht-circle-blank.png'
    for index,color in enumerate(color_list):
        style_normal = simplekml.Style()
        style_normal.iconstyle.icon.href = icon_href
        style_normal.iconstyle.color = color
        style_normal.labelstyle.scale = 0
        style_normal.labelstyle.colormode = None
        style_normal.iconstyle.colormode = None
        style_normal.iconstyle.heading = None

        style_highlight = simplekml.Style()
        style_highlight.iconstyle.icon.href = icon_href
        style_highlight.iconstyle.color = color
        style_highlight.labelstyle.scale = 1
        style_highlight.labelstyle.colorMode = None
        style_highlight.iconstyle.colorMode = None
        style_highlight.iconstyle.heading = None

        style_map = simplekml.StyleMap()
        style_map.normalstyle = style_normal
        style_map.highlightstyle = style_highlight

        style_dict[categories[index]] = style_map
    return style_dict


def rate_to_pct_str(number):
    try:
        number = float(number)
    except ValueError as e:
        return number
    return  round(number*100,2)


def format_dollars_str(number):
    try:
        number = int(number)
    except ValueError as e:
        return number
    return number


def remove_no_data(df_sample,column):
    df_sample[column] = df_sample[column].apply(lambda x:None if x=='SNR' else x)

def generate_dot_map(df,stateName,map_dict):

    kml = simplekml.Kml()
    kml.document.name = "{0} DOT MAP".format(stateName)

    categories = list(map_dict.keys())
    legend_titles = set()
    hasCoord = False

    for index, row in df.iterrows():
        pnt = kml.newpoint(coords=[(row.longitude,row.latitude)])# lon, lat, optional height
        data_pairs = [
            ("Median House Price","${0:,.0f}".format(row.median_house_price)),
            ("Vacancy Rate Monthly","{0:.1f}%".format((row.vanancy_rate_monthly))),
            ("Avg Income PW Gross","${0:,.0f}".format(row.averageIncomePerWeekGross)),
            ("% Rented","{0:.1f}%".format(row.rentedConcentration)),
            ("Ten year growth PA(%)","{0:.1f}".format(row.ten_year_growth))
        ]
        datas = []
        for name,value in data_pairs:
            data = simplekml.Data()
            data.name = name
            data.value = value
            datas.append(data)

        data = simplekml.Data()
        data.name = "TYG Category"

        if row.ten_year_growth < 0:
            data.value = categories[0]
            pnt.stylemap = map_dict.get(data.value)
        elif  0 <= row.ten_year_growth < 1:
            data.value = categories[1]
            pnt.stylemap = map_dict.get(data.value)
        elif  1 <= row.ten_year_growth < 2:
            data.value = categories[2]
            pnt.stylemap = map_dict.get(data.value)
        elif  2 <= row.ten_year_growth < 3:
            data.value = categories[3]
            pnt.stylemap = map_dict.get(data.value)
        elif  3 <= row.ten_year_growth < 4:
            data.value = categories[4]
            pnt.stylemap = map_dict.get(data.value)
        elif  4 <= row.ten_year_growth < 5:
            data.value = categories[5]
            pnt.stylemap = map_dict.get(data.value)
        elif  5 <= row.ten_year_growth < 6:
            data.value = categories[6]
            pnt.stylemap = map_dict.get(data.value)
        elif  6 <= row.ten_year_growth < 7:
            data.value = categories[7]
            pnt.stylemap = map_dict.get(data.value)
        elif  7 <= row.ten_year_growth < 8:
            data.value = categories[8]
            pnt.stylemap = map_dict.get(data.value)
        elif  8 <= row.ten_year_growth < 9:
            data.value = categories[9]
            pnt.stylemap = map_dict.get(data.value)
        elif  9 <= row.ten_year_growth < 10:
            data.value = categories[10]
            pnt.stylemap = map_dict.get(data.value)
        else:
            data.value = categories[11]
            pnt.stylemap = map_dict.get(data.value)

        datas.append(data)
        exdata = simplekml.ExtendedData()
        exdata.datas = datas
        pnt.extendeddata = exdata
        pnt.name = row.nameSuburb

        if not hasCoord:
            coords=[(row.longitude,row.latitude)]
        legend_titles.add(data.value)

    fol = kml.newfolder(name = "Color - Ten Year Growth({0})".format(stateName))
    fol.visibility = 0
    for key,value in map_dict.items():
        if key in legend_titles:
            lgpnt = fol.newpoint(coords=[(row.longitude,row.latitude)])
            lgpnt.visibility = 0
            lgpnt.name = key
            lgpnt.value = None
            lgpnt.stylemap = value

    kml.save('{0}_DOT_MAP.kml'.format(stateName))
    print("{0}'s map generated.\n".format(stateName))
    return kml

def generate_dot_map_qld(df,stateName,map_dict):

    kml = simplekml.Kml()
    kml.document.name = "{0} DOT MAP".format(stateName)

    categories = list(map_dict.keys())
    legend_titles = set()
    hasCoord = False

    for index, row in df.iterrows():
        pnt = kml.newpoint(coords=[(row.longitude,row.latitude)])# lon, lat, optional height
        data_pairs = [
            ("Median House Price","${0:,.0f}".format(row.median_house_price)),
            ("Vacancy Rate Monthly","{0:.1f}%".format((row.vanancy_rate_monthly))),
            ("Avg Income PW Gross","${0:,.0f}".format(row.averageIncomePerWeekGross)),
            ("% Rented","{0:.1f}%".format(row.rentedConcentration)),
            ("Ten year growth PA (%)","{0:.2f}".format(row.ten_year_growth))
        ]
        datas = []
        for name,value in data_pairs:
            data = simplekml.Data()
            data.name = name
            data.value = value
            datas.append(data)

        data = simplekml.Data()
        data.name = "TYG Category"
        ten_yr_gw = row.ten_year_growth
        if row.ten_year_growth <= 0:
            data.value = categories[0]
            pnt.stylemap = map_dict.get(data.value)
        elif  0 < row.ten_year_growth <= 0.49:
            data.value = categories[1]
            pnt.stylemap = map_dict.get(data.value)
        elif  0.49 < row.ten_year_growth <= 0.99:
            data.value = categories[2]
            pnt.stylemap = map_dict.get(data.value)
        elif  0.99 < row.ten_year_growth <= 1.49:
            data.value = categories[3]
            pnt.stylemap = map_dict.get(data.value)
        elif   1.49 < row.ten_year_growth <=1.99:
            data.value = categories[4]
            pnt.stylemap = map_dict.get(data.value)
        elif  1.99 < row.ten_year_growth <= 2.49:
            data.value = categories[5]
            pnt.stylemap = map_dict.get(data.value)
        elif  2.49 < row.ten_year_growth <= 2.99:
            data.value = categories[6]
            pnt.stylemap = map_dict.get(data.value)
        elif  2.99 < row.ten_year_growth <= 3.49:
            data.value = categories[7]
            pnt.stylemap = map_dict.get(data.value)
        elif  3.49 < row.ten_year_growth <= 3.99:
            data.value = categories[8]
            pnt.stylemap = map_dict.get(data.value)
        elif  3.99 < row.ten_year_growth <= 4.49:
            data.value = categories[9]
            pnt.stylemap = map_dict.get(data.value)
        elif  4.49 < row.ten_year_growth <= 4.99:
            data.value = categories[10]
            pnt.stylemap = map_dict.get(data.value)
        elif  4.99 < row.ten_year_growth <= 5.49:
            data.value = categories[11]
            pnt.stylemap = map_dict.get(data.value)
        elif  5.49 < row.ten_year_growth <= 5.99:
            data.value = categories[12]
            pnt.stylemap = map_dict.get(data.value)
        else:
            data.value = categories[13]
            pnt.stylemap = map_dict.get(data.value)

        datas.append(data)
        exdata = simplekml.ExtendedData()
        exdata.datas = datas
        pnt.extendeddata = exdata
        pnt.name = row.nameSuburb

        if not hasCoord:
            coords=[(row.longitude,row.latitude)]
        legend_titles.add(data.value)

    fol = kml.newfolder(name = "Color - Ten Year Growth({0})".format(stateName))
    fol.visibility = 0
    for key,value in map_dict.items():
        if key in legend_titles:
            lgpnt = fol.newpoint(coords=[(row.longitude,row.latitude)])
            lgpnt.visibility = 0
            lgpnt.name = key
            lgpnt.value = None
            lgpnt.stylemap = value

    kml.save('{0}_DOT_MAP.kml'.format(stateName))
    print("{0}'s map generated.\n".format(stateName))
    return kml

def generate_dot_map_nt_wa(df,stateName,map_dict):

    kml = simplekml.Kml()
    kml.document.name = "{0} DOT MAP".format(stateName)

    categories = list(map_dict.keys())
    legend_titles = set()
    hasCoord = False

    for index, row in df.iterrows():
        pnt = kml.newpoint(coords=[(row.longitude,row.latitude)])# lon, lat, optional height
        data_pairs = [
            ("Median House Price","${0:,.0f}".format(row.median_house_price)),
            ("Vacancy Rate Monthly","{0:.1f}%".format((row.vanancy_rate_monthly))),
            ("Avg Income PW Gross","${0:,.0f}".format(row.averageIncomePerWeekGross)),
            ("% Rented","{0:.1f}%".format(row.rentedConcentration)),
            ("Ten year growth PA (%)","{0:.1f}".format(row.ten_year_growth))
        ]
        datas = []
        for name,value in data_pairs:
            data = simplekml.Data()
            data.name = name
            data.value = value
            datas.append(data)

        data = simplekml.Data()
        data.name = "TYG Category"
        ten_yr_gw = row.ten_year_growth
        if row.ten_year_growth <= -4:
            data.value = categories[0]
            pnt.stylemap = map_dict.get(data.value)
        elif  -4 < row.ten_year_growth <= -3:
            data.value = categories[1]
            pnt.stylemap = map_dict.get(data.value)
        elif  -3 < row.ten_year_growth <= -2:
            data.value = categories[2]
            pnt.stylemap = map_dict.get(data.value)
        elif  -2 < row.ten_year_growth <= -1:
            data.value = categories[3]
            pnt.stylemap = map_dict.get(data.value)
        elif   -1 < row.ten_year_growth <=0:
            data.value = categories[4]
            pnt.stylemap = map_dict.get(data.value)
        elif  0 < row.ten_year_growth <= 1:
            data.value = categories[5]
            pnt.stylemap = map_dict.get(data.value)
        elif  1 < row.ten_year_growth <= 2:
            data.value = categories[6]
            pnt.stylemap = map_dict.get(data.value)
        elif  2 < row.ten_year_growth <= 3:
            data.value = categories[7]
            pnt.stylemap = map_dict.get(data.value)
        elif  3 < row.ten_year_growth <= 4:
            data.value = categories[8]
            pnt.stylemap = map_dict.get(data.value)
        else:
            data.value = categories[9]
            pnt.stylemap = map_dict.get(data.value)

        datas.append(data)
        exdata = simplekml.ExtendedData()
        exdata.datas = datas
        pnt.extendeddata = exdata
        pnt.name = row.nameSuburb

        if not hasCoord:
            coords=[(row.longitude,row.latitude)]
        legend_titles.add(data.value)

    fol = kml.newfolder(name = "Color - Ten Year Growth({0})".format(stateName))
    fol.visibility = 0
    for key,value in map_dict.items():
        if key in legend_titles:
            lgpnt = fol.newpoint(coords=[(row.longitude,row.latitude)])
            lgpnt.visibility = 0
            lgpnt.name = key
            lgpnt.value = None
            lgpnt.stylemap = value

    kml.save('{0}_DOT_MAP.kml'.format(stateName))
    print("{0}'s map generated.\n".format(stateName))
    return kml
