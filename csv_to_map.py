import os
import sys

from matplotlib import cm
import pandas as pd
import simplekml

pd.options.mode.chained_assignment = None  # default='warn'


def rate_to_pct_str(number):
    try:
        number = float(number)
    except ValueError as e:
        return number
    return  round(number*100,2)


def format_dollars_str(number):
    try:
        number = float(number)
    except ValueError as e:
        return number
    return int(number)


def remove_no_data(df_sample,column):
    df_sample[column] = df_sample[column].apply(lambda x:None if x=='SNR' else x)

def generate_dot_map(df,stateName,map_dict):

    kml = simplekml.Kml()
    kml.document.name = "{0} DOT MAP".format(stateName)

    legend_titles = set()
    hasCoord = False

    for index, row in df.iterrows():
        pnt = kml.newpoint(coords=[(row.longitude,row.latitude)])# lon, lat, optional height
        data_pairs = [
            ("Median House Price","${0:,.0f}".format(row.median_house_price)),
            ("Vacancy Rate Monthly","{0:.1f}%".format((row.vanancy_rate_monthly))),
            ("Avg Income PW Gross","${0:,.0f}".format(row.averageIncomePerWeekGross)),
            ("% Rented","{0:.1f}%".format(row.rentedConcentration)),
            ("Ten year growth PA","{0:.1f}%".format(row.ten_year_growth))
        ]
        datas = []
        for name,value in data_pairs:
            data = simplekml.Data()
            data.name = name
            data.value = value
            datas.append(data)

        data = simplekml.Data()
        data.name = "TYG Category"

        if row.ten_year_growth < 0:
            data.value = categories[0]
            pnt.stylemap = map_dict.get(data.value)
        elif  0 <= row.ten_year_growth < 1:
            data.value = categories[1]
            pnt.stylemap = map_dict.get(data.value)
        elif  1 <= row.ten_year_growth < 2:
            data.value = categories[2]
            pnt.stylemap = map_dict.get(data.value)
        elif  2 <= row.ten_year_growth < 3:
            data.value = categories[3]
            pnt.stylemap = map_dict.get(data.value)
        elif  3 <= row.ten_year_growth < 4:
            data.value = categories[4]
            pnt.stylemap = map_dict.get(data.value)
        elif  4 <= row.ten_year_growth < 5:
            data.value = categories[5]
            pnt.stylemap = map_dict.get(data.value)
        elif  5 <= row.ten_year_growth < 6:
            data.value = categories[6]
            pnt.stylemap = map_dict.get(data.value)
        elif  6 <= row.ten_year_growth < 7:
            data.value = categories[7]
            pnt.stylemap = map_dict.get(data.value)
        elif  7 <= row.ten_year_growth < 8:
            data.value = categories[8]
            pnt.stylemap = map_dict.get(data.value)
        elif  8 <= row.ten_year_growth < 9:
            data.value = categories[9]
            pnt.stylemap = map_dict.get(data.value)
        elif  9 <= row.ten_year_growth < 10:
            data.value = categories[10]
            pnt.stylemap = map_dict.get(data.value)
        else:
            data.value = categories[11]
            pnt.stylemap = map_dict.get(data.value)

        datas.append(data)
        exdata = simplekml.ExtendedData()
        exdata.datas = datas
        pnt.extendeddata = exdata
        pnt.name = row.nameSuburb

        if not hasCoord:
            coords=[(row.longitude,row.latitude)]
        legend_titles.add(data.value)

    fol = kml.newfolder(name = "Color - Ten Year Growth({0})".format(stateName))
    fol.visibility = 0
    for key,value in map_dict.items():
        if key in legend_titles:
            lgpnt = fol.newpoint(coords=[(row.longitude,row.latitude)])
            lgpnt.visibility = 0
            lgpnt.name = key
            lgpnt.value = None
            lgpnt.stylemap = value

    kml.save('{0}_DOT_MAP.kml'.format(stateName))
    return kml

df = pd.read_excel('input.xlsx')
df_required = df[[
    "nameState",
    "nameSuburb",
    "median_house_price",
    "ten_year_growth",
    "rentedConcentration",
    "sold_per_year",
    "vanancy_rate_monthly",
    "averageIncomePerWeekGross",
    "latitude",
    "longitude",
    "nameCity"
]]

df_required['ten_year_growth'] = df_required['ten_year_growth'].apply(rate_to_pct_str)
df_required['median_house_price'] = df_required['median_house_price'].apply(format_dollars_str)
df_required['vanancy_rate_monthly'].apply(rate_to_pct_str)
df_required['vanancy_rate_monthly'] = df_required['vanancy_rate_monthly'].apply(rate_to_pct_str)
df_required.rentedConcentration = df_required.rentedConcentration.apply(rate_to_pct_str)

for column in df_required:
    remove_no_data(df_required,column)

df_clean = df_required.dropna()

group_obj = df_clean.groupby(by=df_clean.nameState)
dfs = [group_obj.get_group(x) for x in group_obj.groups]
df_act = dfs[0].sort_values('ten_year_growth')
df_nsw = dfs[1].sort_values('ten_year_growth')
df_nt = dfs[2].sort_values('ten_year_growth')
df_qld = dfs[3].sort_values('ten_year_growth')
df_sa = dfs[4].sort_values('ten_year_growth')
df_tas = dfs[5].sort_values('ten_year_growth')
df_vic = dfs[6].sort_values('ten_year_growth')
df_wa = dfs[7].sort_values('ten_year_growth')


color_list = [
    '#ff0000',
    '#ff4824',
    '#ff8a48',
    '#f3c16a',
    '#c5e88a',
    '#97fca7',
    '#68fcc1',
    '#3ae8d7',
    '#0cc1e8',
    '#238af5',
    '#5148fc',
    '#8000ff'
]

categories = [
    'Smaller than 0%',
    '0% to 0.9%',
    '1% to 1.9%',
    '2% to 2.9%',
    '3% to 3.9%',
    '4% to 4.9%',
    '5% to 5.9%',
    '6% to 6.9%',
    '7% to 7.9%',
    '8% to 8.9%',
    '9% to 9.9%',
    'Greater than 10%'
]

style_dict = {}
icon_href = 'http://www.gstatic.com/mapspro/images/stock/959-wht-circle-blank.png'
for index,color in enumerate(color_list):
    style_normal = simplekml.Style()
    style_normal.iconstyle.icon.href = icon_href
    style_normal.iconstyle.color = color
    style_normal.labelstyle.scale = 0
    style_normal.labelstyle.colormode = None
    style_normal.iconstyle.colormode = None
    style_normal.iconstyle.heading = None

    style_highlight = simplekml.Style()
    style_highlight.iconstyle.icon.href = icon_href
    style_highlight.iconstyle.color = color[1:]
    style_highlight.labelstyle.scale = 1
    style_highlight.labelstyle.colorMode = None
    style_highlight.iconstyle.colorMode = None
    style_highlight.iconstyle.heading = None

    style_map = simplekml.StyleMap()
    style_map.normalstyle = style_normal
    style_map.highlightstyle = style_highlight

    style_dict[categories[index]] = style_map

generate_dot_map(df_act,"ACT",style_dict)
generate_dot_map(df_nsw,"New South Wales",style_dict)
generate_dot_map(df_nt, "NT",style_dict)
generate_dot_map(df_qld, "Queensland",style_dict)
generate_dot_map(df_sa, "South Australia",style_dict)
generate_dot_map(df_tas, "Tasmainia",style_dict)
generate_dot_map(df_vic, "Victoria",style_dict)
generate_dot_map(df_wa, "Westren Australia",style_dict)
