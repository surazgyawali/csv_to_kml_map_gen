import os
import sys

import pandas as pd

import utils


pd.options.mode.chained_assignment = None  # default='warn'

df = pd.read_excel('input.xlsx')
df_required = df[[
    "nameState",
    "nameSuburb",
    "median_house_price",
    "ten_year_growth",
    "rentedConcentration",
    "sold_per_year",
    "vanancy_rate_monthly",
    "averageIncomePerWeekGross",
    "latitude",
    "longitude",
    "nameCity"
]]

df_required['ten_year_growth'] = df_required['ten_year_growth'].apply(utils.rate_to_pct_str)
df_required['median_house_price'] = df_required['median_house_price'].apply(utils.format_dollars_str)
df_required['vanancy_rate_monthly'].apply(utils.rate_to_pct_str)
df_required['vanancy_rate_monthly'] = df_required['vanancy_rate_monthly'].apply(utils.rate_to_pct_str)
df_required.rentedConcentration = df_required.rentedConcentration.apply(utils.rate_to_pct_str)

for column in df_required:
    utils.remove_no_data(df_required,column)

df_clean = df_required.dropna()

group_obj = df_clean.groupby(by=df_clean.nameState)
dfs = [group_obj.get_group(x) for x in group_obj.groups]
df_act = dfs[0].sort_values('ten_year_growth')
df_nsw = dfs[1].sort_values('ten_year_growth')
df_nt = dfs[2].sort_values('ten_year_growth')
df_qld = dfs[3].sort_values('ten_year_growth')
df_sa = dfs[4].sort_values('ten_year_growth')
df_tas = dfs[5].sort_values('ten_year_growth')
df_vic = dfs[6].sort_values('ten_year_growth')
df_wa = dfs[7].sort_values('ten_year_growth')

BLACK="5a14001E"
L_BLUE="5fF0FF14"
M_BLUE="5fF08C14"
D_BLUE="5fF00014"
D_PURPLE="5f781478"
L_PURPLE="5fFF78B4"
L_GREEN="5f78FF00"
D_GREEN="5f14B43C"
YELLOW="5f14F0FF"
L_YELLOW = "5f78FFF0"
ORANGE="5f1478FF"
L_RED="5fB478F0"
RED="5f7828F0"
B_RED="5a1400FF"

color_list = [
    BLACK,
    L_BLUE,
    M_BLUE,
    D_BLUE,
    D_PURPLE,
    L_PURPLE,
    L_GREEN,
    D_GREEN,
    YELLOW,
    ORANGE,
    RED,
    B_RED
]

categories = [
    'Smaller than 0%',
    '0% to 0.9%',
    '1% to 1.9%',
    '2% to 2.9%',
    '3% to 3.9%',
    '4% to 4.9%',
    '5% to 5.9%',
    '6% to 6.9%',
    '7% to 7.9%',
    '8% to 8.9%',
    '9% to 9.9%',
    'Greater than 10%'
]

categories_qld = [
    'Smaller than 0%',
    '0% to 0.4%',
    '0.5% to 0.9%',
    '1% to 1.4%',
    '1.5% to 1.9%',
    '2% to 2.4%',
    '2.5% to 2.9%',
    '3% to 3.4%',
    '3.5% to 3.9%',
    '4% to 4.4%',
    '4.5% to 4.9%',
    '5% to 5.4%',
    '5.5% to 5.9%',
    'Greater than 6%'
]

color_list_qld = [
    BLACK,
    L_BLUE,
    M_BLUE,
    D_BLUE,
    D_PURPLE,
    L_PURPLE,
    L_GREEN,
    D_GREEN,
    YELLOW,
    L_YELLOW,
    ORANGE,
    L_RED,
    RED,
    B_RED
]

categories_nt_wa = [
    'Smaller than -4%',
    '-3.9% to -3%',
    '-2.9% to -2%',
    '-1.9% to -1%',
    '-0.9% to 0%',
    '0% to 1%',
    '1.1% to 2%',
    '2.1% to 3%',
    '3.1% to 4%',
    'Greater than 4%'
]

color_list_nt_qa = [
    BLACK,
    L_BLUE,
    D_BLUE,
    L_PURPLE,
    L_GREEN,
    D_GREEN,
    YELLOW,
    ORANGE,
    RED,
    B_RED
]

style_dict = utils.generate_style_map(color_list,categories)
style_dict_qld = utils.generate_style_map(color_list_qld,categories_qld)
style_dict_nt_wa = utils.generate_style_map(color_list_nt_qa,categories_nt_wa)
# as it is
utils.generate_dot_map(df_act,"ACT",style_dict)
utils.generate_dot_map(df_nsw,"New South Wales",style_dict)
utils.generate_dot_map(df_tas, "Tasmainia",style_dict)
utils.generate_dot_map(df_vic, "Victoria",style_dict)

#custom legend
utils.generate_dot_map_nt_wa(df_nt, "NT",style_dict_nt_wa)
utils.generate_dot_map_nt_wa(df_wa, "Westren Australia",style_dict_nt_wa)
utils.generate_dot_map_qld(df_qld, "Queensland",style_dict_qld)
utils.generate_dot_map_nt_wa(df_sa, "South Australia",style_dict_nt_wa)
input("\n\nCompleted, Press Enter to exit!! :)")